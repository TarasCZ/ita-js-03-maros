import React from "react";
import {ContractsService} from '../../Service/contract-service';
import {ContractForm} from './contract-form';

export class ContractAdd extends React.Component {

    handleCreate(newData) {
        this.create(newData);
    }

    async create(newData) {
        await ContractsService.create(newData);
        this.props.history.push('/contract');
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row main-heading">
                    <h1 className="text-center">Add contract</h1>
                </div>
                <ContractForm handleFormSubmit={this.handleCreate.bind(this)}/>
            </div>
        );
    }
}

