import React from "react";
import _ from 'lodash';
import {Link} from 'react-router-dom';
import {ContractListItem} from "./contract-listItem";
import {ContractsService} from '../../Service/contract-service';

export class ContractList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.loadContracts();
    }

    async loadContracts() {
        const allData = await ContractsService.getContracts();
        this.setState({
            data: allData
        });
    }

    getSearchContract() {
        return _.filter(this.state.data, d=> JSON.stringify(_.values(d)).toLowerCase().includes(this.props.search.toLowerCase()));
    }

    render() {
        const users = this.getSearchContract().map((user) =>

            <ContractListItem key={user.id} url={`/contract/detail/${user.id}`} name={user.name} price={user.price}
                      note={user.note}/>
        );

        return (
            <div className="container-fluid">

                <div>
                    <div className="row main-heading">
                        <div className=" col-md-offset-4 col-md-4">
                            <h1 className="text-center">Contracts</h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-6">
                            <Link to="/contract/add">
                                <button className="btn btn-success"> New Contract
                                </button>
                            </Link>
                        </div>
                    </div>
                    <div className="row main-contents">
                        <div className="col-xs-12">
                            <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Note</th>
                                </tr>
                                </thead>
                                <tbody>
                                {users}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
