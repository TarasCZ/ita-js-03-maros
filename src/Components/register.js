import React from 'react';
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form';
import {User} from '../Service/user-service';

const validate = values => {

    const errors = {};
    if (!values.username) {
        errors.username = 'Username required'
    } else if (values.username.length < 4) {
        errors.username = 'Username must be 4 characters or more'
    }
    if (!values.email) {
        errors.email = 'Email required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address'
    }
    if (!values.password) {
        errors.password = 'Password required'
    }
    if (!values.password2) {
        errors.password2 = 'Repeat password required'
    } else if (values.password !== values.password2) {
        errors.password2 = 'Repeat passwords not matched!'
    }
    return errors
};

const asyncValidate = async (values) => {
    let usernames = [];
    const result = await User.getUsernames();
    if (result) {
        result.forEach((r) => {
            usernames.push(r.username);
        })
    }
    if (usernames.includes(values.username)) {
        let error = {username: 'That username is taken'};
        throw error;
    }
};

const renderField = ({input, label, type, meta: {asyncValidating, touched, error,}}) => (
    <div>
        {touched && error && <div className="alert alert-danger">
            <strong>Wrong!</strong> {error}
        </div>}
        <label>{label}</label>
        <div className="form-group">
            <input className="form-control" {...input} type={type} placeholder={label}/>
        </div>
    </div>
);

export class Register extends React.Component {
    render() {
        const {handleSubmit, submitting} = this.props;
        return (
            <div className="container-fluid">
                <div className="row">
                    <h1 className="text-center">Register</h1>
                </div>
                <div className="row">
                    <div className="col-xs-offset-3 col-xs-6">
                        <form onSubmit={handleSubmit}>
                            <Field name="username" type="text" component={renderField} label="Username"/>
                            <Field name="email" type="email" component={renderField} label="Email"/>
                            <Field name="password" type="password" component={renderField} label="Password"/>
                            <Field name="password2" type="password" component={renderField} label="Repeat password"/>
                            <div>
                                <button className="btn btn-success" type="submit" disabled={submitting}>Register
                                </button>
                                <Link className="btn btn-info main-btn" to='/login'>Go to login</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

Register = reduxForm({
    form: 'Register',
    validate,
    asyncValidate
})(Register);