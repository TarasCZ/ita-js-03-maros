import {createStore, combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

const rootReducer = (state = {
    search: "",
    UserLoggedInID: localStorage.getItem('id')
}, action) => {
    switch (action.type) {

        case 'SWITCH_LOGGED_IN':
            localStorage.setItem('id',JSON.stringify(action.payload));
            const user = localStorage.getItem('id');
                state = {
                ...state,
                UserLoggedInID: user
            };
            break;

        case 'SEARCH':
            state = {
                ...state,
                search: action.payload
            };
            break;

        default:
            return state;
    }

    return state;
};

const store = createStore(
    combineReducers({root: rootReducer, form: formReducer})
);

export default store;



